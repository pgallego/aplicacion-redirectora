# Aplicación Redirectora

Construir un programa en Python que sirva cualquier invocación que se le realice con una redirección (códigos de resultado HTTP en el rango 3xx) a otro recurso (aleatorio), que bien puede ser de sí mismo o externo a partir de una lista con URLs.
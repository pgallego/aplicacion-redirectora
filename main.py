import socket
import random
import sys

urls = ["https://www.elmundo.es/", "https://elpais.com/", "https://www.marca.com/", "https://www.larazon.es/",
        "https://www.eldiario.es/", "https://www.elconfidencial.com/", "https://www.lavanguardia.com/"]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(("127.0.0.1", int(sys.argv[1])))
s.listen(5)

try:
    while True:
        (recvs, address) = s.accept()
        received = recvs.recv(2048).decode()
        print(f"HTTP request received:\n{received}")

        ext_random = random.choice(urls)
        int_random = f"http://localhost:{sys.argv[1]}/{random.randint(1, 100000)}"

        choice = random.choice([ext_random, int_random])

        response = 'HTTP/1.1 302 Moved Temporarily \r\n\r\n' \
                   + '<html><head><meta http-equiv="refresh" content="2;url=' + choice + '"</head>' \
                   + '<body><h1>Redirigiendo a ' + choice + '</h1>' \
                   + '</body></hmtl>' + '\r\n'

        recvs.send(response.encode('utf-8'))
        recvs.close()
except KeyboardInterrupt:
    print("\nUser terminated the program (Ctrl+C)")
    sys.exit(0)
